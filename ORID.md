# Daily Report (2023/07/17)
# O (Objective): 
## 1. What did we learn today?
### We learned refactoring and code smell today.

## 2. What activities did you do? 
### I conducted a code review with the team members and presented the results of last week's group assignment and practiced refactoring.

## 3. What scenes have impressed you?
### I have been impressed by the presentation with my team members today.

---------------------

# R (Reflective): 
## Please use one word to express your feelings about today's class.
---------------------
### interesting.

# I (Interpretive):
## What do you think about this? What was the most meaningful aspect of this activity?
---------------------
### I think the most meaningful aspect of this activity is showing the results of our homework last week with the team members. Because this not only let me have a deeper understanding of the observer pattern which is one of the behavioral design pattern, but also let me exercise my expression ability and enhance my self-confidence.


# D (Decisional): 
## Where do you most want to apply what you have learned today? What changes will you make?
---------------------
### I will use Java stream instead of loo and try to reduce the use of if else statements as much as possible when I write the functions, and I try to apply design patterns to code development.In addition,I will try to refactor my code after I pass the test.
### I will develop a good coding habit of committing in BB steps. And I will strengthen communication with group members when doing group assignments in order to set aside time to rehearse the content of the speech in advance and present better performance results.



