import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String CALCULATE_ERROR = "Calculate Error";

    public String getResult(String inputStr) {
        try {
            List<WordFrequency> wordFrequencyList = getWordFrequencies(inputStr);
            Map<String, List<WordFrequency>> inputGroup = groupInputByWord(wordFrequencyList);
            List<WordFrequency> getWordFrequenciesWithCount = getWordFrequenciesWithCount(inputGroup);
            sortWordFrequency(getWordFrequenciesWithCount);
            return getWordFrequenciesString(getWordFrequenciesWithCount);
        } catch (Exception e) {
            return CALCULATE_ERROR;
        }
    }

    private static void sortWordFrequency(List<WordFrequency> getWordFrequenciesWithCount) {
        getWordFrequenciesWithCount.sort((w1, w2) -> w2.getWordCount() - w1.getWordCount());
    }

    private static String getWordFrequenciesString(List<WordFrequency> getWordFrequenciesWithCount) {
        StringJoiner joiner = new StringJoiner("\n");
        getWordFrequenciesWithCount.stream().forEach(wordFrequency -> joiner.add(wordFrequency.getWord() + " " + wordFrequency.getWordCount()));
        return joiner.toString();
    }

    private static List<WordFrequency> getWordFrequenciesWithCount(Map<String, List<WordFrequency>> map) {
        List<WordFrequency> list = new ArrayList<>();
        for (Map.Entry<String, List<WordFrequency>> entry : map.entrySet()) {
            WordFrequency wordFrequency = new WordFrequency(entry.getKey(), entry.getValue().size());
            list.add(wordFrequency);
        }
        return list;
    }

    private static List<WordFrequency> getWordFrequencies(String inputStr) {
        String[] inputs = inputStr.split("\\s+");
        return Arrays.stream(inputs).map(input -> new WordFrequency(input, 1)).collect(Collectors.toList());
    }

    private Map<String, List<WordFrequency>> groupInputByWord(List<WordFrequency> wordFrequencyList) {
        return wordFrequencyList.stream()
                .collect(Collectors.groupingBy(WordFrequency::getWord));
    }
}
